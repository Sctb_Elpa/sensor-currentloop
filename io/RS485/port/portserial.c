/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: portserial.c,v 1.1 2006/08/22 21:35:13 wolti Exp $
 */

#include "port.h"
#include <delays.h>

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"
#include "shaduler.h"
#include "system.h"
#include "dataModel.h"


/* ----------------------- static variables ---------------------------------*/

/* ----------------------- functions ---------------------------------*/

/* ----------------------- Start implementation -----------------------------*/

uint32_t speed;

void disableRessiver() {
    while (TXSTA1bits.TRMT == 0);
    RECEIVE_ON_485();
    if (processAfterAnsver) {
        shadule(processAfterAnsver);
        processAfterAnsver = NULL;
    }
}

void
vMBPortSerialEnable(BOOL xRxEnable, BOOL xTxEnable) {
    /* If xRXEnable enable serial receive interrupts. If xTxENable enable
     * transmitter empty interrupts.
     */
    if (xRxEnable == TRUE) {
        RCSTA1bits.CREN1 = 1;
        PIE1bits.RC1IE = 1;
        //485 status determined by xTxEnable state*/
    } else {
        RCSTA1bits.CREN1 = 0;
        PIE1bits.RC1IE = 0;
        //485 status determined by xTxEnable state
    }
    if (xTxEnable == TRUE) {
        PIE1bits.TX1IE = 1;
        TRANSMIT_ON_485();
        //Allow time for bus to turn around.
        __delay_us(10);
    } else {
        PIE1bits.TX1IE = 0;
        shadule_isr(disableRessiver);
    }
}

BOOL
xMBPortSerialInit(UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity) {
    USHORT devider;
    // prevent compiler warning.
    (void) ucPORT;
    (void) eParity;
    // Only supporting RTU so always 8 data bits
    (void) ucDataBits;

    speed = ulBaudRate;

    // setup RS485 direction control pin
    clear_bit(DIRECTION_485_TRIS, TRANSMIT_485_BIT);

    RCSTA1bits.SPEN1 = 1;
    
    //Int priority low
    IPR1bits.RC1IP = 0;
    IPR1bits.TX1IP = 0;

    TRISCbits.TRISC6 = 0; // tx - output
    TRISCbits.TRISC7 = 1; // rx - input

#if 1
    TXSTA1bits.BRGH = 1; // high baud rate

    BAUDCON1bits.BRG161 = 1; //Set BRG16 bit
    //Set 16 bit baudrate count
    devider = (SYS_FREQ / (4 * ulBaudRate)) - 1;
    SPBRG1 = (UCHAR) devider;
    SPBRGH1 = (devider >> 8) && 0xff;
#else
    BAUDCON2bits.BRG162 = 1; //Set BRG16 bit
    //Set 16 bit baudrate count
    devider = (F_CPU / (16 * ulBaudRate)) - 1;
    SPBRG2 = (UCHAR) devider;
    SPBRGH2 = (devider >> 8) && 0xff;
#endif

    //Leave transmit enabled
    TXSTA1bits.TXEN1 = 1;

    vMBPortSerialEnable(FALSE, FALSE);
    RECEIVE_ON_485();
    return TRUE;
}

BOOL
xMBPortSerialPutByte(CHAR ucByte) {
    /* Put a byte in the UARTs transmit buffer. This function is called
     * by the protocol stack if pxMBFrameCBTransmitterEmpty( ) has been
     * called. */

    //Write the data byte
    TXREG1 = ucByte;

    return TRUE;
}

BOOL
xMBPortSerialGetByte(CHAR * pucByte) {
    /* Return the byte in the UARTs receive buffer. This function is called
     * by the protocol stack
     */
    *pucByte = RCREG1;
    return TRUE;
}

void vMBPortClose(void) {
}
