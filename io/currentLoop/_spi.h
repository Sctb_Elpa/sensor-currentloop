#ifndef _SPI_H_
#define _SPI_H_

#define SS_PORT             LATC
#define SS_DIR              TRISC
#define SS_PIN              2

#include "port.h"

void _spi_init();
UCHAR _spi_xfer(UCHAR* buf, UCHAR count);
void _spi_xfer_async(UCHAR* buf, UCHAR count);
void SPI_isr();

#endif /* _SPI_H_ */