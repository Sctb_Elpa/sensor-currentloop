/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
#include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include "_spi.h"

#include "port.h"
#include "dataModel.h"

#include "currentLoop.h"

#define DAC_ERROR_LVL_LOW(x)            (0x2500 - x)
#define DAC_ERROR_LVL_HIGH(x)           (0xe500 + x)

#define CONFIG_DAC_ERROR_LVL_HIGH(x)    0xe5 + x, 0x00
#define CONFIG_DAC_ERROR_LVL_LOW(x)     0x25 - x, 0

// DAC registers

/* When PROTECT_REG_WR is set to 1, then a XFER_REG command is necessary to 15:0
 * XFER[15:0] transfer the previous register write data into the appropriate
 * address. Set this register to 0x00FF to perform a XFER_REG command. */
#define REG_XFER                        0x01
#define REG_XFER_VALUE                  0x00FF

/* No Operation. A write to this register  will not change any device
 * configuration.*/
#define REG_NOP                         0x02

// to enable protected write mode
#define REG_WR_MODE                     0x03
#define REG_WR_MODE_SIMPLE              0x0000
#define REG_WR_MODE_PROTECTED           0x0001

// output code
#define REG_DACCODE                     0x04

#define REG_ERR_CONFIG                  0x05

struct REG_ERR_CONFIGbits {
    // LOOP Retry time = (L_RETRY_TIME + 1) � 50 ms
    unsigned L_RETRY_TIME : 3;
    
    // unused
    unsigned : 5;

    //--------------------------------------------------------------------------

    /* Disable SPI timeout
     * A SPI timeout error drives ERRB low when a SPI Timeout error occurs */
    unsigned MASK_SPI_TOUT : 1;
    
    /* SPI Timeout = (SPI_TIMEOUT+ 1) � 50 ms */
    unsigned SPI_TIMEOUT : 3;
    
    /* SPI timeout errors do not change the OUT pin current to an error value */
    unsigned MASK_SPI_ERR : 1;
    
    // When a LOOP error is detected the DAC161S997 does not drive ERRB pin low */
    unsigned DIS_LOOP_ERR_ERRB : 1;
    
    /* When a Loop Error is detected the DAC161S997 tries to maintain DACCODE
     * current on pin OUT */
    unsigned MASK_LOOP_ERR : 1;
    
    /*  Do not periodically reassert DACCODE output when a loop error is
     * present; reassert DACCODE after STATUS Register is read out */
    unsigned DIS_RETRY_LOOP : 1;
};
#define REG_ERR_LOW                     0x06
// if (error_low) REG_DACCODE <= REG_ERR_LOW & 0xff00

#define REG_ERR_HIGH                    0x07
// if (error_high) REG_DACCODE <= REG_ERR_HIGH & 0xff00

#define REG_RESET                       0x08
#define REG_RESET_RESET_CODE            0xC33C

#define REG_STATUS                      0x09
struct REG_STATUSbits {
    // A loop error is occurring
    unsigned CURR_LOOP_STS : 1;

    // Loop status STICKY BIT
    unsigned LOOP_STS : 1;

    // SPI WRITE time out error
    unsigned SPI_TIMEOUT_ERR : 1;

    // Frame-error status STICKY BIT
    unsigned FERR_STS : 1;

    // Returns the state of the ERRLVL pin
    unsigned ERRLVL_PIN : 1;

    // DAC resolution == 0b111
    unsigned DAC_RES : 3;

    // unused
    unsigned : 8;
};

#define READ_REG(reg)                   (reg | (1 << 7))

enum enControlStates {
    STATE_IDLE,
    STATE_CHECK_ENABLED,
    STATE_STATUS_REQUEST,
    STATE_STATUS_READ,
    STATE_STATUS_PROCESS_CTRL_WRITE,
    STATE_SHUTDOWN
};

static enum enControlStates ControlState;
static float mA2Codecoeff;

void current_Loop_updateDAC_Coeff() {
    mA2Codecoeff = ((float)(RegHoldingBuf.names.DACValue20mA -
                            RegHoldingBuf.names.DACValue4mA)) / (20.0 - 4.0);
}

void currentLoop_init() {
    _spi_init();

    // error lvl pin = out
    set_bit(ERR_LVL_PORT, ERR_LVL_PIN); // error = error_high
    clear_bit(ERR_LVL_DIR, ERR_LVL_PIN);

    // dac error = in
    set_bit(DAC_ERR_IN_DIR, DAC_ERR_IN_PIN);

    // configure DAC

    UCHAR cmd_set_err_lvl_high[] = {REG_ERR_HIGH, CONFIG_DAC_ERROR_LVL_HIGH(0)};
    _spi_xfer(cmd_set_err_lvl_high, sizeof (cmd_set_err_lvl_high));
    UCHAR cmd_set_err_lvl_low[] = {REG_ERR_LOW, CONFIG_DAC_ERROR_LVL_LOW(0)};
    _spi_xfer(cmd_set_err_lvl_low, sizeof (cmd_set_err_lvl_high));

    union {
        struct {
            uint8_t cmd;
            struct REG_ERR_CONFIGbits values;
        };
        uint8_t raw[3];
    } err_config;

    err_config.cmd = REG_ERR_CONFIG;

    err_config.values.SPI_TIMEOUT = 1;
    err_config.values.DIS_RETRY_LOOP = 0;
    err_config.values.L_RETRY_TIME = 0;
    err_config.values.MASK_LOOP_ERR = 1;
    err_config.values.MASK_SPI_ERR = 1;
    err_config.values.MASK_SPI_TOUT = 1;

    _spi_xfer(err_config.raw, sizeof (err_config));

    // set transfer mode (protected/not)
    UCHAR cmd_set_simple_mode[] = {REG_WR_MODE, 0, 0};
    _spi_xfer(cmd_set_simple_mode, sizeof (cmd_set_simple_mode));

    UCHAR disabe_dac[] = {REG_DACCODE, 0, 0};
	_spi_xfer(disabe_dac, sizeof (disabe_dac));	

    current_Loop_updateDAC_Coeff();

    ControlState = STATE_CHECK_ENABLED;
}

uint8_t currentLoop_isDACERROR() {
    return !!(DAC_ERR_IN_PORT & (1 << DAC_ERR_IN_PIN));
}

void currentLoop_setErrLvl(uint8_t lvl) {
    if (lvl)
        set_bit(ERR_LVL_PORT, ERR_LVL_PIN);
    else
        clear_bit(ERR_LVL_PORT, ERR_LVL_PIN);
}

void currentLoop_control() {
    static volatile UCHAR spi_buf[3];
    static uint8_t was_enabled = 0;
    switch (ControlState) {
        case STATE_CHECK_ENABLED:
#if 0
            ControlState = STATE_SHUTDOWN;
            return;        
#endif
            if (!CoilsBuf.names.PowerSaveMode ) {
                ControlState = STATE_STATUS_REQUEST;
                was_enabled = TRUE;
            }
            else if (was_enabled) {
                ControlState = STATE_SHUTDOWN;
                was_enabled = FALSE;
            }
            return;
        case STATE_STATUS_REQUEST:
            spi_buf[0] = READ_REG(REG_STATUS);
            spi_buf[1] = 0;
            spi_buf[2] = 0;
            ControlState = STATE_STATUS_READ;
            break;
        case STATE_STATUS_READ:
            spi_buf[0] = READ_REG(REG_STATUS);
            spi_buf[1] = 0;
            spi_buf[2] = 0;
            ControlState = STATE_STATUS_PROCESS_CTRL_WRITE;
            break;
        case STATE_STATUS_PROCESS_CTRL_WRITE:
        {
            struct REG_STATUSbits *status = (struct REG_STATUSbits*)&spi_buf[2];
            DiscretInputsBuf.names.CurrentLoopError = status->LOOP_STS;

            union {
                uint16_t U16;
                uint8_t U8[2];
            } DAC_Value;
            if (RegHoldingBuf.names.DACTestValue) {
                DAC_Value.U16 = RegHoldingBuf.names.DACTestValue;
                DiscretInputsBuf.names.LowPreassure = FALSE;
                DiscretInputsBuf.names.HighPreassure = FALSE;
            } else do {
                float P = RegHoldingBuf.names.CurrentLoopTestPressure;
                if (P == 0.0) {
                    P = RegInputBuf.names.Pressure;
                }
                if (DiscretInputsBuf.names.LowPreassure =
                        (P < RegHoldingBuf.names.Pi0)) {
                    DAC_Value.U16 = DAC_ERROR_LVL_LOW(0);
                    break;
                }
                uint32_t t = (uint32_t)((P - RegHoldingBuf.names.Pi0) *
                        RegHoldingBuf.names.P2Icoeff * mA2Codecoeff);
                if (DiscretInputsBuf.names.HighPreassure = 
                   (t > (RegHoldingBuf.names.DACValue20mA - RegHoldingBuf.names.DACValue4mA))) {
                    DAC_Value.U16 = DAC_ERROR_LVL_HIGH(0);
                } else {
                    DAC_Value.U16 = (uint16_t)t + RegHoldingBuf.names.DACValue4mA;
                }
            } while(0);

#if 1
            if (RegInputBuf.names.CurrentLoopValue != DAC_Value.U16) {
                RegInputBuf.names.CurrentLoopValue = DAC_Value.U16; // to output
                spi_buf[0] = REG_DACCODE;
                spi_buf[1] = DAC_Value.U8[1];
                spi_buf[2] = DAC_Value.U8[0];
            } else {
                spi_buf[0] = REG_NOP;
            }
#else
            spi_buf[0] = REG_DACCODE;
            spi_buf[1] = DAC_Value.U8[1];
            spi_buf[2] = DAC_Value.U8[0];
#endif
            
            ControlState = STATE_CHECK_ENABLED;
        }
            break;
        case STATE_SHUTDOWN:
            spi_buf[0] = REG_DACCODE;
            spi_buf[1] = 0;
            spi_buf[2] = 0;
            RegInputBuf.names.CurrentLoopValue = 0;
            ControlState = STATE_CHECK_ENABLED;
            break;
        default:
            ControlState = STATE_CHECK_ENABLED;
            return;
    }
    _spi_xfer/*_async*/((UCHAR*)spi_buf, sizeof(spi_buf));
}