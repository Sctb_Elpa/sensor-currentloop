#ifndef _CURRENT_LOOP_H_
#define _CURRENT_LOOP_H_

#include <stdint.h>

#define ERR_LVL_PIN                 1
#define ERR_LVL_PORT                LATC
#define ERR_LVL_DIR                 TRISC

#define DAC_ERR_IN_PIN              0
#define DAC_ERR_IN_PORT             PORTC
#define DAC_ERR_IN_DIR              TRISC


#define REG_DACCODE_4MA             0x2AAA
#define REG_DACCODE_20MA            0xD555

void currentLoop_init();

uint8_t currentLoop_isDACERROR();
void currentLoop_setErrLvl(uint8_t lvl);
void currentLoop_control();
void current_Loop_updateDAC_Coeff();

#endif /* _CURRENT_LOOP_H_ */