/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
#include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include <spi.h>

#endif


#include "_spi.h"


void _spi_init() {
    OpenSPI(
#ifdef USE_PLL
            SPI_FOSC_16, // SPI Master mode, clock = Fosc/16
#else
            SPI_FOSC_4,  // SPI Master mode, clock = Fosc/4
#endif
            MODE_11, // spi mode
            SMPMID // read on mode (test needed)
            );
    DisableIntSPI;
    SPI_Clear_Intr_Status_Bit;
    SetPriorityIntSPI(0);
    SSPCON1bits.SSPOV = 0;

    // configure SS
    set_bit(SS_PORT, SS_PIN);
    clear_bit(SS_DIR, SS_PIN);
}

UCHAR _spi_xfer(UCHAR* buf, UCHAR count) {
    UCHAR transmitted = 0;
    SSPBUF;                       // Clears BF
    SPI_Clear_Intr_Status_Bit;
    SSPCON1bits.WCOL = 0;         // Clear any previous write collision

    clear_bit(SS_PORT, SS_PIN);   // /SS
    while (count--) {
        SSPBUF = *buf;            // write byte to SSPBUF register
        if (SSPCON1bits.WCOL)     // collision?
            break;
        while (!SPI_Intr_Status) {// wait
            CLRWDT();
            eMBPoll();
        }
        SPI_Clear_Intr_Status_Bit;
        *buf = SSPBUF;            // read ressived
        ++buf;
        ++transmitted;
    }
    set_bit(SS_PORT, SS_PIN);     // ^SS
    return transmitted;
}

static UCHAR* io_pointer;
static UCHAR remamning;

void _spi_xfer_async(UCHAR* buf, UCHAR count) {
    SSPBUF;                       // Clears BF
    SSPCON1bits.WCOL = 0;         // Clear any previous write collision
    
    clear_bit(SS_PORT, SS_PIN);   // /SS
    io_pointer = buf;
    remamning = count - 1;
    
    SSPBUF = *io_pointer;
    if (SSPCON1bits.WCOL)         // collision?
        return;

    SPI_Clear_Intr_Status_Bit;
    EnableIntSPI;
}

void SPI_isr() {
    *io_pointer++ = SSPBUF;       // read
    if (remamning--) {
        SSPBUF = *io_pointer;
        if (!SSPCON1bits.WCOL) {  // collision?
            return;
        }
    }
    // finish
    DisableIntSPI;
    set_bit(SS_PORT, SS_PIN); // ^SS
}