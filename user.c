#include "core.h"
#include "mb.h"
#include "port.h"
#include "FreqMeters.h"
#include "shaduler.h"
#include "dataModel.h"
#include "currentLoop.h"

#include "user.h"

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

/* <Initialize variables in user.h and insert code for user algorithms.> */

void initMB() {
    eMBErrorCode err;
    if (RegHoldingBuf.names.Speed == 0xffff)
        err = eMBInit(MB_RTU, RegHoldingBuf.names.Adress, 1,
            115200, MB_PAR_NONE);
    else
        err = eMBInit(MB_RTU, RegHoldingBuf.names.Adress, 1,
            RegHoldingBuf.names.Speed, MB_PAR_NONE);
    if (err == MB_EINVAL)
        // try default
        eMBInit(MB_RTU, DEVICE_ADDR_DEFAULT, 1, DEVICE_SPEED_DEFAULT,
            MB_PAR_NONE);
}

void InitApp(void) {
    initCore();
    
        /* Disable unnessusary modules*/
    // disable analog inputs
    ANCON0 = 0;
    ANCON1 = 0;
    CCP3CONbits.CCP3M = 0;
    CCP4CONbits.CCP4M = 0;

    PMD0 = ~((1 << 0) | (1 << 1)); // disable all but UART1 and MSSP
    PMD1 = (1 << 7) | (1 << 6) | (1 << 5); // disable PSP, CTMU, ADC
    PMD2 = 0b1111; // disable all

    /* Initialize modbus */
    initMB();

    /* Initialize currentLoop */
    currentLoop_init();

    // sleep mode PRI_IDLE
    OSCCONbits.IDLEN = 1;
    OSCCONbits.SCS = 0b00;

    /* Configure the IPEN bit (1=on) in RCON to turn on/off int priorities */
    RCONbits.IPEN = 1;
}

extern UCHAR ucMBAddress;

void applySettings() {
    INTCONbits.GIEH = 0;
    
    mark_unstable();

    // apply variables
    ApplyHoldings();

    // restart modbus
    if ((speed == 115200 && RegHoldingBuf.names.Speed != 0xffff) ||
            speed != RegHoldingBuf.names.Speed ||
            RegHoldingBuf.names.Adress != ucMBAddress) {
        eMBDisable();
        eMBClose();
        initMB();
        eMBEnable();
    }

    current_Loop_updateDAC_Coeff();
    
#if 0
    // restart counters
    if (!CoilsBuf.names.PowerSaveMode) {
        // forse restart
        CoilsBuf.names.PowerSaveMode = 1;
        FreqMetersEnable();
        CoilsBuf.names.PowerSaveMode = 0;
        FreqMetersEnable();
    } else {
        FreqMetersEnable();
    }
#endif

    INTCONbits.GIEH = 1;
}

void processTimeCrytical() {
    CLRWDT();
    eMBPoll();
    ProcessShaduled(); // exec shaduled task if needed
}

void UserMain() {
    CalcT();
    processTimeCrytical();
    CalcP();
    processTimeCrytical();
    
    if (CoilsBuf.names.PowerSaveMode) {
        SLEEP();
    } else {
        check_limits();
    }
}
