/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
#include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp */
#include "mb.h"
#include "port.h"
#include "dataModel.h"
#include "shaduler.h"

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/

/******************************************************************************/
/* Main Program                                                               */

/******************************************************************************/

void main(void) {
    ENTER_CRITICAL_SECTION();
    /* Configure the oscillator for the device */
    ConfigureOscillator();

    /* Initialize I/O and Peripherals for application */

#if 0
    TRISCbits.TRISC6 = 0;
    LATCbits.LATC6 = 1;
#endif

    InitApp();

    WDTCONbits.SWDTE = 1;

    //DiscretInputsBuf.names.I2CMode = 1;
    //CoilsBuf.names.InfinitySendMode  = 1;
    //CoilsBuf.names.TemperatureEnabled = 0;
    eMBEnable();

    /* Enable interrupts */
    EXIT_CRITICAL_SECTION();
#if !FAST_TIMER_ISR
    INTCONbits.GIEH = 1;
#endif
    while (1) {
        UserMain();
    }
}

