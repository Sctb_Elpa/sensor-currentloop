/* 
 * File:   shaduler.h
 * Author: tolyan
 *
 * Created on 11 ???? 2015 ?., 17:05
 */

#ifndef SHADULER_H
#define	SHADULER_H

typedef void (*taskRoutine)(void);

struct Task
{
    taskRoutine Routine;
    uint16_t period;
};

void enableShaduler(BOOL enable);
void tick();
void ProcessShaduled();
BOOL shadule(taskRoutine routine);
BOOL shadule_isr(taskRoutine routine);
BOOL shadule_calc(taskRoutine routine);

#endif	/* SHADULER_H */

