/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
#include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include <string.h>

#include "system.h"
#include "core.h"
#include "infintySender.h"
#include "dataModel.h"
#include "shaduler.h"
#include "currentLoop.h"
#include "port.h"

#define MIN(x, y)	((x < y) ? x : y)

// using TIMER4 for shaduling on 1ms

//#define TOGGLE

#ifdef TOGGLE

static void toggle() {
    TRISCbits.TRISC6 = 0;
    LATCbits.LATC6 = !LATCbits.LATC6;
}
#endif

static volatile struct Task tasks[] ={
    {
        sendReport, 20
    },
    {
        checkChanelsAlive, 400
    },
    {
        mark_stabelised, 1000
    },
#if 1
    {
        currentLoop_control, 20
    }
#endif
#ifdef TOGGLE
    ,
    {
        toggle, 1
    }
#endif
};

static volatile struct Task tasks_work[sizeof (tasks) / sizeof (struct Task) ];

static uint8_t taskQueue_wp = 0;
static uint8_t taskQueue_rp = 0;
static uint8_t taskQueue_used = 0;
static taskRoutine taskQueue[32];

#define MODULE_CLOCK            (FCY)
#define TARGET_COUNTER_TICKS_MS (MODULE_CLOCK / 1000)

static void reloadTask(uint8_t id) {
    memcpy(&tasks_work[id], & tasks[id], sizeof (struct Task));
}

void enableShaduler(BOOL enable) {
    if (enable) {
        uint8_t postscaler;
        uint8_t prescaler;

        tasks[0].period = RegHoldingBuf.names.cyclicalSendPeriod;

        for (prescaler = 0; prescaler < sizeof (tasks) / sizeof (struct Task);
                ++prescaler)
            reloadTask(prescaler);

        // autoselect prescaler and postscaler
        genPrescalerPostscalerPlank(TARGET_COUNTER_TICKS_MS, &prescaler,
                &postscaler, (unsigned char*) &PR4);

        T4CON = 0;

        T4CONbits.T4OUTPS = postscaler;
        T4CONbits.T4CKPS = prescaler;

        TMR4 = 0;

        IPR4bits.TMR4IP = 0; // low prio
        PIR4bits.TMR4IF = 0;
        PIE4bits.TMR4IE = 1;

        T4CONbits.TMR4ON = 1; // enable
    } else {
        T4CONbits.TMR4ON = 0;
        PIE4bits.TMR4IE = 0;
    }
}

void tick() {
    for (uint8_t TaskID = 0; TaskID < sizeof (tasks) / sizeof (struct Task);
            ++TaskID) {
        taskRoutine Routine = tasks_work[TaskID].Routine;
        if (Routine && (--tasks_work[TaskID].period == 0)) {
            tasks_work[TaskID].period = tasks[TaskID].period; // reload
            shadule_isr(Routine);
        }
    }
}

void ProcessShaduled() {
    while (taskQueue_used) {
        CLRWDT();
        taskRoutine shaduledRoutine = taskQueue[taskQueue_rp++];
        if (taskQueue_rp == sizeof (taskQueue) / sizeof (taskRoutine))
            taskQueue_rp = 0;
        --taskQueue_used;
        shaduledRoutine();
    }
}

BOOL shadule(taskRoutine routine) {
    BOOL res;

    if (taskQueue_used == sizeof (taskQueue) / sizeof (taskRoutine))
        res = FALSE;
    else {
        taskQueue[taskQueue_wp++] = routine;
        if (taskQueue_wp == sizeof (taskQueue) / sizeof (taskRoutine))
            taskQueue_wp = 0;

        ++taskQueue_used;

        res = TRUE;
    }

    return res;
}

BOOL shadule_isr(taskRoutine routine) {
    BOOL res;

    if (taskQueue_used == sizeof (taskQueue) / sizeof (taskRoutine))
        res = FALSE;
    else {
        taskQueue[taskQueue_wp++] = routine;
        if (taskQueue_wp == sizeof (taskQueue) / sizeof (taskRoutine))
            taskQueue_wp = 0;

        ++taskQueue_used;

        res = TRUE;
    }

    return res;
}

BOOL shadule_calc(taskRoutine routine) {
    BOOL res;

    if (taskQueue_used == sizeof (taskQueue) / sizeof (taskRoutine))
        res = FALSE;
    else {
        uint8_t i = taskQueue_rp;
        while (i != taskQueue_wp) {
            if (taskQueue[i] == routine)
                return true;
            if (++i == sizeof (taskQueue) / sizeof (taskRoutine))
                i = 0;
        }

        taskQueue[taskQueue_wp++] = routine;
        if (taskQueue_wp == sizeof (taskQueue) / sizeof (taskRoutine))
            taskQueue_wp = 0;

        ++taskQueue_used;

        res = TRUE;
    }

    return res;
}