/*
 * File:   modbus.h
 * Author: tolyan
 *
 * Created on 10 ???? 2015 ?., 10:14
 */

#ifndef MODBUS_H
#define	MODBUS_H

#include <stdint.h>

#define DEVICE_ID                       0xDB0E
#define DEVICE_ADDR_DEFAULT             4
#define DEVICE_SPEED_DEFAULT            57600
/************************************************************************/

#define REG_INPUT_START 		(0)
#define REG_INPUT_NREGS 		(4 * 2 + 1)

#define REG_HOLDING_START 		(0)
#define REG_HOLDING_NREGS 		(12 + 5 * 2 + 13 * 2)

#define COILS_START			(0)
#define COILS_NCOILS			(10)

#define DISCRET_INPUTS_START            (0)
#define DISCRET_INPUTS_NINPUTS          (6)

#ifndef test_bit
#define test_bit(REG, n)                (REG & (1 << n))
#endif

#define PTR_TO_REGINDEX(p)              (p >> 1) /* ==> p / sizeof(uint16_t)*/

enum enMeasureUnits {
    MU_kgs_cm2 = 0,
    MU_mmHg = 1,
    MU_gPa = 2,
    MU_kPa = 3,
    MU_MPa = 4,
    MU_mmH2O = 5,
    MU_psi = 6,

    MU_COUNT,
};

struct sTemperatureCoeffs {
    float T0;
    float C[3];
    float F0;
};

struct sPressureCoeffs {
    float A[6];
    float Ft0;
    float Fp0;
};

#if REG_HOLDING_NREGS > 0

union unHoldings {

    struct {
        uint16_t ID;
        uint16_t Serial;
        uint16_t Password;
        uint16_t PressureMesureTime;
        uint16_t TemperatureMesureTime;
        uint16_t MeasureUnits;

        struct sPressureCoeffs P_coeffs;
        struct sTemperatureCoeffs T_coeffs;

        uint16_t Adress;
        uint16_t Speed;
        uint16_t cyclicalSendPeriod;

        float    Pi0;
        float    P2Icoeff;
        uint16_t DACValue4mA;
        uint16_t DACValue20mA;

        // test
        float    CurrentLoopTestPressure;
        uint16_t DACTestValue;

        // protection
        float    OverheatValue;
        float    OverPresssValue;
    } names;
    uint16_t raw[REG_HOLDING_NREGS];
};
extern union unHoldings RegHoldingBuf;
extern union unHoldings RegHoldingBuf_tmp;
extern const uint8_t ucRegHoldongROMask[];
#endif

#if COILS_NCOILS > 0

union unCoils {

    struct {
        unsigned PresureEnabled : 1;
        unsigned TemperatureEnabled : 1;
        unsigned WriteSettings : 1;
        unsigned ApplySettings : 1;
        unsigned InfinitySendMode : 1;
        unsigned PowerSaveMode : 1;
        unsigned ResetDefaults : 1;

        unsigned DisableBreakLogging : 1;
        unsigned OverheatDetected : 1;
        unsigned OverPresssDetected : 1;
    } names;
    uint8_t raw[(COILS_NCOILS - 1) / 8 + 1];
};
extern union unCoils CoilsBuf;
#endif

#if DISCRET_INPUTS_NINPUTS > 0

union unDiscretInputs {

    struct {
        unsigned PresureChanelFailure : 1;
        unsigned TemperatureChanelFailure : 1;
        unsigned CryticalTemperature : 1;
        unsigned LowPreassure: 1;
        unsigned HighPreassure: 1;
        unsigned CurrentLoopError: 1;
    } names;
    uint8_t raw[(DISCRET_INPUTS_NINPUTS - 1) / 8 + 1];
};
extern union unDiscretInputs DiscretInputsBuf;
#endif

#if REG_INPUT_NREGS > 0

union unInputs {

    struct _names {
        float Pressure;
        float Temperature;
        float Fp;
        float Ft;

        uint16_t CurrentLoopValue;
    } names;
    uint16_t raw[REG_INPUT_NREGS];
};
extern union unInputs RegInputBuf;
#endif

struct EEPROMContainer {
    uint16_t CRC;
#if REG_HOLDING_NREGS > 0
    union unHoldings holdings;
#endif
#if COILS_NCOILS > 0
    union unCoils Coils;
#endif
};


#ifndef _MB_H

typedef enum {
    MB_ENOERR, /*!< no error. */
    MB_ENOREG, /*!< illegal register address. */
    MB_EINVAL, /*!< illegal argument. */
    MB_EPORTERR, /*!< porting layer error. */
    MB_ENORES, /*!< insufficient resources. */
    MB_EIO, /*!< I/O error. */
    MB_EILLSTATE, /*!< protocol stack in illegal state. */
    MB_ETIMEDOUT /*!< timeout error occurred. */
} eMBErrorCode;

typedef enum {
    MB_REG_READ, /*!< Read register values and pass to protocol stack. */
    MB_REG_WRITE /*!< Update register values. */
} eMBRegisterMode;

eMBErrorCode eMBRegInputCB(uint8_t * pucRegBuffer, uint16_t usAddress,
        uint16_t usNRegs);
eMBErrorCode eMBRegHoldingCB(uint8_t * pucRegBuffer, uint16_t usAddress,
        uint16_t usNRegs, eMBRegisterMode eMode);
eMBErrorCode eMBRegCoilsCB(uint8_t * pucRegBuffer, uint16_t usAddress,
        uint16_t usNCoils, eMBRegisterMode eMode);
eMBErrorCode eMBRegDiscreteCB(uint8_t * pucRegBuffer, uint16_t usAddress,
        uint16_t usNDiscrete);
#endif

void processCoils(union unCoils newCoilsSet);

void InitDataModel();
void saveSettings();
eMBErrorCode holdingValidator();

float convertkgs_m2_TO(float mmHg);

uint8_t __div8(uint8_t val);
uint8_t __mod8(uint8_t val);

extern void (*processAfterAnsver)(void);

#endif	/* MODBUS_H */

