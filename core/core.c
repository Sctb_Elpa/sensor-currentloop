/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
#include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include <string.h>
#include <stdlib.h>

#include "mb.h"
#include "FreqMeters.h"
#include "dataModel.h"
#include "system.h"
#include "shaduler.h"
#include "user.h"

#include "core.h"

#define FILTER_ENABLED 			0
#define SUCCESS_LOOPS_NEEDED            3

struct Filter {
    const uint8_t size;
    const float const *coeffs;
    float *data;
    const float k;
};

static unsigned long maxU = ~0;
#define NAN  	(*((float*)&maxU))

static const float filterCoeffs[] = {
    0.08855240952789907,
    0.10950406583046077,
    0.1433572578657607,
    0.15619928299052657,
    0.1433572578657607,
    0.10950406583046077,
    0.08855240952789907,
};

#define FilterSumm  0.839026749

static BOOL stabelised = FALSE;

static float PFilter_data[sizeof (filterCoeffs) / sizeof (float) ];
static float TFilter_data[sizeof (filterCoeffs) / sizeof (float) ];

static struct Filter PFilter = {
    sizeof (filterCoeffs) / sizeof (float), filterCoeffs, PFilter_data, FilterSumm
};
static struct Filter TFilter = {
    sizeof (filterCoeffs) / sizeof (float), filterCoeffs, TFilter_data, FilterSumm
};

void initCore() {
    InitDataModel();
    FreqMetersEnable();
    enableShaduler(1);
}

void checkChanelsAlive() {
#if 1
    DiscretInputsBuf.names.PresureChanelFailure = 0;
    DiscretInputsBuf.names.TemperatureChanelFailure = 0;
    if (CoilsBuf.names.PowerSaveMode) {
        // no mesure - no errors ;)
        DiscretInputsBuf.names.PresureChanelFailure = 0;
        RegInputBuf.names.Fp = 0.0;
        RegInputBuf.names.Pressure = NAN;
        
        DiscretInputsBuf.names.TemperatureChanelFailure = 0;
        DiscretInputsBuf.names.CryticalTemperature = 0;
        RegInputBuf.names.Ft = 0.0;
        RegInputBuf.names.Temperature = NAN;
    } else {
        if (CoilsBuf.names.PresureEnabled) {
            if (!PresAliveCounter) {
                DiscretInputsBuf.names.PresureChanelFailure = 1; // error detected
                RegInputBuf.names.Pressure = NAN;
                RegInputBuf.names.Fp = 0.0;
            } else
                --PresAliveCounter;
        } else {
            DiscretInputsBuf.names.PresureChanelFailure = 0;
            RegInputBuf.names.Fp = 0.0;
            RegInputBuf.names.Pressure = NAN;
        }

        if (CoilsBuf.names.TemperatureEnabled) {
            if (!TempAliveCounter) {
                DiscretInputsBuf.names.TemperatureChanelFailure = 1; // error detected
                RegInputBuf.names.Temperature = NAN;
                RegInputBuf.names.Ft = 0.0;
            } else
                --TempAliveCounter;

            DiscretInputsBuf.names.CryticalTemperature =
                    RegInputBuf.names.Temperature >= CRITICAL_TEMPERATURE;
        } else {
            DiscretInputsBuf.names.TemperatureChanelFailure = 0;
            DiscretInputsBuf.names.CryticalTemperature = 0;
            RegInputBuf.names.Ft = 0.0;
            RegInputBuf.names.Temperature = NAN;
        }
    }
#endif
}

static bool detctCatcheerror(uint16_t val) {
    return (val < 0xff);
}

uint32_t calcTargetvalue(uint32_t F, uint16_t mesureTime_ms) {
    uint32_t res = (F * mesureTime_ms) / 1000;
    return res ? res : 1;
}

static float fir_filter(struct Filter* filter, float curent) {
#if FILTER_ENABLED == 1
    memmove(&filter->data[1], &filter->data[0], (filter->size - 1) * sizeof (float));
    filter->data[0] = curent;
#if 0
    printf("{");
    for (uint8_t i = 0; i < filter->size; ++i) {
        printf("%f,", filter->data[i]);
    }
    printf("}\n");
#endif
    float result = 0;
    for (uint8_t i = 0; i < filter->size; ++i) {
        result += filter->data[i] * filter->coeffs[i];
    }
    return result / filter->k;
#else
    return curent;
#endif
}

/* DEBUGER shows, what TMR0H fails to write, 8-bit mode set */
void CalcP() {
    if ((!T0CONbits.TMR0ON) &&
            CoilsBuf.names.PresureEnabled &&
            !CoilsBuf.names.PowerSaveMode) {
        uint32_t result;
        static uint32_t target = 0;
        static uint8_t Redy = 0;
        float Fp;

#if 0
        RegInputBuf.names.start = pressureData.masterFreqData[0].value;
        RegInputBuf.names.stop = pressureData.masterFreqData[1].value;
#endif

        bool err = 0;
        err |= detctCatcheerror(pressureData.masterFreqData[0].s.REGISTER);
        err |= detctCatcheerror(pressureData.masterFreqData[1].s.REGISTER);
        if (err && target) {
            PresTimerTarget_new.value = target;
            pressureData.work = PresTimerTarget_new.s.Extantion;
            PresTimerTarget_new.s.REGISTER = (~PresTimerTarget_new.s.REGISTER) + 1;
            TMR0L = 0xff;
            T0CONbits.TMR0ON = 1; // start new cycle
            PresAliveCounter = ALIVE_COUNTER;
            return;
        }

        result = pressureData.masterFreqData[1].value -
                pressureData.masterFreqData[0].value;
        if (result & (1ul << (sizeof (result) * 8 - 1)))
            result = (1ul << (sizeof (result) * 8 - 1)) -
            pressureData.masterFreqData[0].value +
                pressureData.masterFreqData[1].value;

        if (!DiscretInputsBuf.names.PresureChanelFailure) {
            Fp = MASTER_CLOCK_HZ / (float) result * target;
            PresTimerTarget_new.value = calcTargetvalue(
                    (uint32_t) Fp,
                    RegHoldingBuf.names.PressureMesureTime);

            if (Redy < SUCCESS_LOOPS_NEEDED)
                ++Redy;
        } else {
            PresTimerTarget_new.value = calcTargetvalue(
                    (uint32_t) START_FREQ,
                    RegHoldingBuf.names.PressureMesureTime);
            Redy = 0;
        }

        target = PresTimerTarget_new.value; // for next calculations
        pressureData.work = PresTimerTarget_new.s.Extantion;
        PresTimerTarget_new.s.REGISTER = (~PresTimerTarget_new.s.REGISTER) + 1;
        TMR0H = 0xff;
        NOP();
        TMR0L = 0xf0;
        T0CONbits.TMR0ON = 1; // start new cycle

        processTimeCrytical();
#if 1
        // calc
        if (!DiscretInputsBuf.names.PresureChanelFailure
                && (Redy == SUCCESS_LOOPS_NEEDED)) {
            float Fp_f = fir_filter(&PFilter, Fp);

            struct sPressureCoeffs PressureCoeffs;
            memcpy(&PressureCoeffs, &RegHoldingBuf.names.P_coeffs,
                    sizeof (struct sPressureCoeffs));

            float Ft_minus_Ft0;
            if (CoilsBuf.names.TemperatureEnabled)
                Ft_minus_Ft0 = RegInputBuf.names.Ft - PressureCoeffs.Ft0;
            else
                Ft_minus_Ft0 = 0;
            float PresF_minus_Fp0 = Fp_f - PressureCoeffs.Fp0;

            float Pressure_fSens = PressureCoeffs.A[0] +
                    PressureCoeffs.A[5] * Ft_minus_Ft0 * PresF_minus_Fp0;

            processTimeCrytical();

            for (unsigned char i = 1;
                    i < 3;
                    ++i, Ft_minus_Ft0 *= Ft_minus_Ft0,
                    PresF_minus_Fp0 *= PresF_minus_Fp0
                    )
                Pressure_fSens += PressureCoeffs.A[i] * Ft_minus_Ft0
                    + PressureCoeffs.A[i + 2] * PresF_minus_Fp0;

            RegInputBuf.names.Pressure = convertkgs_m2_TO(Pressure_fSens);
            RegInputBuf.names.Fp = Fp_f;
        }
#else
        RegInputBuf.names.Fp = Fp;
        RegInputBuf.names.Pressure = result;
#endif
    }
}

void CalcT() {
    if ((!T1CONbits.TMR1ON) &&
            CoilsBuf.names.TemperatureEnabled &&
            !CoilsBuf.names.PowerSaveMode) {
        uint32_t result;
        static uint32_t target1 = 0;
        static uint8_t Redy = 0;
        float Ft;

#if 0
        RegInputBuf.names.start = temperatureData.masterFreqData[0].value;
        RegInputBuf.names.stop = temperatureData.masterFreqData[1].value;
#endif

        bool err = 0;
        err |= detctCatcheerror(temperatureData.masterFreqData[0].s.REGISTER);
        err |= detctCatcheerror(temperatureData.masterFreqData[1].s.REGISTER);
        if (err && target1) {
            TempTimerTarget_new.value = target1;
            temperatureData.work = TempTimerTarget_new.s.Extantion;
            TempTimerTarget_new.s.REGISTER = (~TempTimerTarget_new.s.REGISTER) + 1;
            TMR1H = 0xff;
            TMR1L = 0xff;
            T1CONbits.TMR1ON = 1; // start new cycle
            TempAliveCounter = ALIVE_COUNTER;
            return;
        }

        result = temperatureData.masterFreqData[1].value -
                temperatureData.masterFreqData[0].value;
        if (result & (1ul << (sizeof (result) * 8 - 1)))
            result = (1ul << (sizeof (result) * 8 - 1)) -
            temperatureData.masterFreqData[0].value +
                temperatureData.masterFreqData[1].value;

        if (result == temperatureData.masterFreqData[0].value) { // cycle force restart?
            TempTimerTarget_new.value = calcTargetvalue(
                    (uint32_t) START_FREQ,
                    RegHoldingBuf.names.TemperatureMesureTime);
        } else if (!DiscretInputsBuf.names.TemperatureChanelFailure) {
            Ft = MASTER_CLOCK_HZ / (float) result * target1;
            TempTimerTarget_new.value = calcTargetvalue(
                    (uint32_t) Ft,
                    RegHoldingBuf.names.TemperatureMesureTime);
            if (Redy < SUCCESS_LOOPS_NEEDED)
                ++Redy;
        } else {
            TempTimerTarget_new.value = calcTargetvalue(
                    (uint32_t) START_FREQ,
                    RegHoldingBuf.names.TemperatureMesureTime);
            Redy = 0;
        }
        target1 = TempTimerTarget_new.value; // for next calculations

        temperatureData.work = TempTimerTarget_new.s.Extantion;
        TempTimerTarget_new.s.REGISTER = (~TempTimerTarget_new.s.REGISTER) + 1;
        
        TMR1H = 0xff;
        TMR1L = 0xff;
        T1CONbits.TMR1ON = 1; // start new cycle

        processTimeCrytical();
#if 1
        // calc
        if (!DiscretInputsBuf.names.TemperatureChanelFailure
                && (Redy == SUCCESS_LOOPS_NEEDED)) {
            // filter nidddles
            float Ft_f = fir_filter(&TFilter, Ft);
            struct sTemperatureCoeffs TemperatureCoeffs;
            memcpy(&TemperatureCoeffs, &RegHoldingBuf.names.T_coeffs,
                    sizeof (struct sTemperatureCoeffs));
            float TempF_minus_Ft0 = Ft - TemperatureCoeffs.F0;
            float _temp = TempF_minus_Ft0;
            float Temperature_fSens = TemperatureCoeffs.T0;

            processTimeCrytical();

            for (unsigned char i = 0; i < 3; ++i, _temp *= TempF_minus_Ft0)
                Temperature_fSens += TemperatureCoeffs.C[i] * _temp;

            RegInputBuf.names.Temperature = Temperature_fSens;
            RegInputBuf.names.Ft = Ft_f;
        }
#else
        RegInputBuf.names.Ft = Ft;
        RegInputBuf.names.Fp = result;
#endif
    }
}

void genPrescalerPostscalerPlank(uint32_t Kd, uint8_t* PrescalerVal,
        uint8_t *postscalerVal, uint8_t *plank) {
    static const uint8_t prescalers[] = {1, 4, 16};
    uint8_t postscaler;
    uint8_t i;
    uint8_t _plank = 0xff;

    for (i = 0; i < sizeof (prescalers); ++i)
        for (postscaler = 1; postscaler <= 16; ++postscaler) {
            uint16_t need_dev = Kd / (prescalers[i] * postscaler);
            if (need_dev < 256) {
                _plank = need_dev;
                goto __end_genPrescalerPostscalerStart;
            }
        }

__end_genPrescalerPostscalerStart:
    *PrescalerVal = i;
    *postscalerVal = postscaler - 1;
    *plank = _plank;
}


void mark_stabelised() {
    stabelised = TRUE;
}

void mark_unstable() {
    stabelised = FALSE;
}

void check_limits() {
    if (stabelised) {
        BOOL detected = FALSE;
        if (!CoilsBuf.names.OverPresssDetected && 
                (RegInputBuf.names.Pressure > RegHoldingBuf.names.OverPresssValue)) {
            CoilsBuf.names.OverPresssDetected = 1;
            detected = TRUE;
        }
        if (!CoilsBuf.names.OverheatDetected && 
                (RegInputBuf.names.Temperature > RegHoldingBuf.names.OverheatValue)) {
            CoilsBuf.names.OverheatDetected = 1;
            detected = TRUE;
        }

        if (detected && !CoilsBuf.names.DisableBreakLogging) {
            union unCoils c = CoilsBuf;
            c.names.WriteSettings = 1;
            processCoils(c);
        }
    }
}